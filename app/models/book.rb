class Book < ActiveRecord::Base
  belongs_to :user
  has_many :meetings, dependent: :destroy
  has_many :comments, dependent: :destroy
  
  acts_as_taggable

  def available?
  	current_meeting.nil?
  end

  def current_meeting
  	meetings.on_date.first
  end

  def late?
    !available? and (Time.now > (current_meeting.start_datetime + 1.hour))
  end

end
