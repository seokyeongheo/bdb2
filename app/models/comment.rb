class Comment < ActiveRecord::Base
  belongs_to :book

  validates :body, presence: true
end
